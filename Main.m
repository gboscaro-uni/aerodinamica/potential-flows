function Main
clear all;

%Var
indisturbato = 0;
sorgente = 0;
vortice = 0;
dipolo = 0;

vinf = 1;
alpha = 0;

n_sor = 2;
n_vor = 1;
n_dip = 1;

sor_first = 0;
vor_first = 0;
dip_first = 0;

x0_sor = []; y0_sor = []; Q_sor = [];
x0_vor = []; y0_vor = []; G_vor = [];
x0_dip = []; y0_dip = []; Q_dip = [];

% Create figure
h.f = figure('units','pixels','position',[600,600,330,120],...
             'toolbar','none','menu','none','name','Moto Composto','numbertitle','off','resize','off');
         
% Create togglebuttons
h.c(1) = uicontrol('style','togglebutton','units','pixels',...
                'position',[10,85,70,25],'string','Indisturbato','callback',@indisturbato_call);
                function indisturbato_call(hObject,eventData)
                    indisturbato = get(hObject,'Value');
                    if(indisturbato)                        
                        set(h.c(5),'Enable','on');
                        set(h.c(10),'Enable','on');
                        set(h.c(6),'Enable','on');
                        set(h.c(11),'Enable','on');
                    else                                                
                        set(h.c(5),'Enable','off');
                        set(h.c(10),'Enable','off');
                        set(h.c(6),'Enable','off');
                        set(h.c(11),'Enable','off');
                    end
                    
                end

h.c(2) = uicontrol('style','togglebutton','units','pixels',...
                'position',[90,85,70,25],'string','Sorgente','callback',@sorgente_call); 
                function sorgente_call(hObject,eventData)
                    sorgente = get(hObject,'Value');
                    if(sorgente)                        
                        set(h.c(7),'Enable','on');
                        set(h.c(12),'Enable','on');
                    else                                                
                        set(h.c(7),'Enable','off');
                        set(h.c(12),'Enable','off');
                    end
                end
h.c(3) = uicontrol('style','togglebutton','units','pixels',...
                'position',[170,85,70,25],'string','Vortice','callback',@vortice_call); 
                function vortice_call(hObject,eventData)
                    vortice = get(hObject,'Value');
                    if(vortice)                        
                        set(h.c(8),'Enable','on');
                        set(h.c(13),'Enable','on');
                    else                                                
                        set(h.c(8),'Enable','off');
                        set(h.c(13),'Enable','off');
                    end
                end 
h.c(4) = uicontrol('style','togglebutton','units','pixels',...
                'position',[250,85,70,25],'string','Dipolo','callback',@dipolo_call); 
                function dipolo_call(hObject,eventData)
                    dipolo = get(hObject,'Value');
                    if(dipolo)                        
                        set(h.c(9),'Enable','on');
                        set(h.c(14),'Enable','on');
                    else                                                
                        set(h.c(9),'Enable','off');
                        set(h.c(14),'Enable','off');
                    end
                end 
            
% Create textbuttons            
%moto indisturbato
h.c(10) = uicontrol('style','text','units','pixels',...
                'position',[10,65,70,20],'string','V infinito','Enable','off');              
h.c(5) = uicontrol('style','edit','units','pixels',...
                'position',[10,50,70,20],'string',vinf,'Enable','off');     
h.c(11) = uicontrol('style','text','units','pixels',...
                'position',[10,25,70,20],'string','Alpha','Enable','off');  
h.c(6) = uicontrol('style','edit','units','pixels',...
                'position',[10,10,70,20],'string',alpha,'Enable','off'); 
            
h.c(12) = uicontrol('style','text','units','pixels',...
                'position',[90,65,70,20],'string','N sorgenti','Enable','off'); 
h.c(7) = uicontrol('style','edit','units','pixels',...
                'position',[90,50,70,20],'string',n_sor,'Enable','off'); 
            
h.c(13) = uicontrol('style','text','units','pixels',...
                'position',[170,65,70,20],'string','N vortici','Enable','off');             
h.c(8) = uicontrol('style','edit','units','pixels',...
                'position',[170,50,70,20],'string',n_vor,'Enable','off');
            
h.c(14) = uicontrol('style','text','units','pixels',...
                'position',[250,65,70,20],'string','N dipoli','Enable','off');             
h.c(9) = uicontrol('style','edit','units','pixels',...
                'position',[250,50,70,20],'string',n_dip,'Enable','off'); 
            
% Create pushbutton   
h.p = uicontrol('style','pushbutton','units','pixels',...
                'position',[170,10,70,35],'string','Cancel',...
                'callback',@esc_call);
h.p = uicontrol('style','pushbutton','units','pixels',...
                'position',[250,10,70,35],'string','OK',...
                'callback',@ok_call);
    % Pushbutton callback
    function esc_call(varargin)
        close(h.f);
    end
    function ok_call(varargin)
        vinf = (round(str2double(get(h.c(5),'String')),2));
        alpha = (round(str2double(get(h.c(6),'String')),2))*(2*pi)/360;
        n_sor = (round(str2double(get(h.c(7),'String')),2));
        n_vor = (round(str2double(get(h.c(8),'String')),2));
        n_dip = (round(str2double(get(h.c(9),'String')),2));
        close(h.f);
        if((not(sorgente) && not(vortice) && not(dipolo)) || (sum([n_sor,n_vor,n_dip] == 0)==3)) %se nessun moto e' abilitato non serve aprire l'altro popup
            moto_composto([indisturbato,sorgente,vortice,dipolo],[vinf,alpha],[],[],[]);
        else
            if(not(sorgente))
                n_sor = 0;
            end
            if(not(vortice))
                n_vor = 0;
            end
            if(not(dipolo))
                n_dip = 0;
            end
            popup_call();
        end
    end
    function popup_call(varargin)
        
        m = max([n_sor,n_vor,n_dip]);
        l = sum([n_sor,n_vor,n_dip] == 0);
        l = 3 - l;
        if not(m == 0)
            %style
            input_height = 20;     
            input_width = 70;
            button_height = 35;
            button_width = 70;
            offset = 10;
            window_height = 5*offset + m*30 + 2*input_height + button_height;
            window_width = 250*l;
            el = 1; ww = 0; 
            
            
            h.f = figure('units','pixels','position',[600,600,window_width,window_height],...
                 'toolbar','none','menu','none','name','Coordinate Punti','numbertitle','off','resize','off');
             
            if(sorgente && not(n_sor==0))
                
                y = window_height - input_height -10;                
                h.c(el) = uicontrol('style','text','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','SORGENTI'); 
                y = y - input_height -10;          
                h.c(el+1) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+offset),y,input_width,input_height],'string','x0');  
                h.c(el+2) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','y0');  
                h.c(el+3) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string','Q');  
                y = y - input_height -10;
                el = el+4;
                
                x0_sor = zeros(1,n_sor); y0_sor = zeros(1,n_sor); Q_sor = zeros(1,n_sor);                
                v = (1:1:n_sor);                
                sor_first = el;
                for i = v
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+offset),y,input_width,input_height],'string',x0_sor(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string',y0_sor(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string',Q_sor(i)); 
                    el = el +1;
                    y = y - input_height -10;
                end
                ww = ww +250;
            end
            if(vortice && not(n_vor==0))
                y = window_height - input_height -10;                
                h.c(el) = uicontrol('style','text','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','VORTICI'); 
                y = y - input_height -10;          
                h.c(el+1) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+offset),y,input_width,input_height],'string','x0');  
                h.c(el+2) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','y0');  
                h.c(el+3) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string','Gamma');  
                y = y - input_height -10;
                el = el+4;
                
                x0_vor = zeros(1,n_vor); y0_vor = zeros(1,n_vor); G_vor = zeros(1,n_vor);                
                v = (1:1:n_vor);         
                vor_first = el;
                for i = v
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+offset),y,input_width,input_height],'string',x0_vor(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string',y0_vor(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string',G_vor(i)); 
                    el = el +1;
                    y = y - input_height -10;
                end
                vor_last = el;
                ww = ww +250;
            end
            
            if(dipolo && not(n_dip==0))
                y = window_height - input_height -10;                
                h.c(el) = uicontrol('style','text','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','DIPOLO'); 
                y = y - input_height -10;          
                h.c(el+1) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+offset),y,input_width,input_height],'string','x0');  
                h.c(el+2) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+input_width+2*offset),y,input_width,input_height],'string','y0');  
                h.c(el+3) = uicontrol('style','text','units','pixels',...
                    'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string','K');  
                y = y - input_height -10;
                el = el+4;
                
                x0_dip = zeros(1,n_dip); y0_dip = zeros(1,n_dip); Q_dip = zeros(1,n_dip);                
                v = (1:1:n_dip);   
                dip_first = el;
                for i = v
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+offset),y,input_width,input_height],'string',x0_dip(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+input_width+2*offset),y,input_width,input_height],'string',y0_dip(i));     
                    el = el +1;
                    h.c(el) = uicontrol('style','edit','units','pixels',...
                            'position',[(ww+2*input_width+3*offset),y,input_width,input_height],'string',Q_dip(i)); 
                    el = el +1;
                    y = y - input_height -10;
                end
                ww = ww +250;
            end
            
            h.p = uicontrol('style','pushbutton','units','pixels',...
                'position',[(ww/2-button_width/2),offset,button_width,button_height],'string','OK',...
                'callback',@fun_call);     
        end
    end
    function fun_call(varargin)

        %sorgenti        
        if(sorgente)
            ee = sor_first;
            for i = 1:1:n_sor
                x0_sor(i) = (round(str2double(get(h.c(ee),'String')),1));
                y0_sor(i) = (round(str2double(get(h.c(ee+1),'String')),1));
                Q_sor(i) = (round(str2double(get(h.c(ee+2),'String')),2));
                ee = ee +3;
            end
        end
        
        %vortici
        if(vortice)
            ee = vor_first;
            for i = 1:1:n_vor
                x0_vor(i) = (round(str2double(get(h.c(ee),'String')),1));
                y0_vor(i) = (round(str2double(get(h.c(ee+1),'String')),1));
                G_vor(i) = (round(str2double(get(h.c(ee+2),'String')),2));
                ee = ee +3;
            end
        end
        
        %dipoli
        if(dipolo)
            ee = dip_first;
            for i = 1:1:n_dip
                x0_dip(i) = (round(str2double(get(h.c(ee),'String')),1));
                y0_dip(i) = (round(str2double(get(h.c(ee+1),'String')),1));
                Q_dip(i) = (round(str2double(get(h.c(ee+2),'String')),2));
                ee = ee +3;
            end
        end
        close(h.f);
        moto_composto([indisturbato,sorgente,vortice,dipolo],[vinf,alpha],{x0_sor,y0_sor,Q_sor},{x0_vor,y0_vor,G_vor},{x0_dip,y0_dip,Q_dip});
    end       
end