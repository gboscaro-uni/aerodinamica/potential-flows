function [] = moto_composto(moti,ind,sor,vor,dip)
    
    x = -3:0.01:3;
    y = -3:0.01:3;
    [X,Y] = meshgrid(x,y);
    
    %--- MOTO INDISTURBATO ---
    PSII = zeros(length(x),length(y));
    PHII = zeros(length(x),length(y));
    UI = zeros(length(x),length(y));
    VI = zeros(length(x),length(y));
    
    if(moti(1))

        vinf = ind(1);
        alpha = ind(2);
        f_phi_ind = @(x,y) vinf*(x*cos(alpha)+y*sin(alpha));
        f_psi_ind = @(x,y) vinf*(y*cos(alpha)-x*sin(alpha));
        vx = vinf*cos(alpha);
        vy = vinf*sin(alpha);
        UI = ones(length(x),length(y))*vx;
        VI = ones(length(x),length(y))*vy;
        
        for i=1:1:length(x)
            
            for j=1:1:length(y)
                
                if(moti(1))
                    
                    PSII(i,j) = feval(f_psi_ind,x(i),y(j));
                    PHII(i,j) = feval(f_phi_ind,x(i),y(j));
                end
            end
        end
    end
    
    %--- MOTO SORGENTE/POZZO ---
    PSIS = zeros(length(x),length(y));
    PHIS = zeros(length(x),length(y));
    US = zeros(length(x),length(y));
    VS = zeros(length(x),length(y));
    
    if(moti(2))
        
        xs = sor{1};
        ys = sor{2};
        Qs = sor{3};
        n_sor = length(Qs);
 
        for i=1:1:length(x)

            for j=1:1:length(y)
                
                for k=1:1:n_sor

                    c = Qs(k)/(2*pi);
                    r = sqrt((x(i)-xs(k))^2+(y(j)-ys(k))^2); 
                    theta = atan2(y(j)-ys(k),x(i)-xs(k));

                    PSIS(i,j) = PSIS(i,j) + c*theta;
                    PHIS(i,j) = PHIS(i,j) + c*log(r);
                    US(i,j) = US(i,j) + c*(x(i)-xs(k))/r^2;
                    VS(i,j) = VS(i,j) + c*(y(j)-ys(k))/r^2;
                end

            end
        end
    end
    
    % --- MOTO VORTICE ---
    PSIV = zeros(length(x),length(y));
    PHIV = zeros(length(x),length(y));
    UV = zeros(length(x),length(y));
    VV = zeros(length(x),length(y));
    
    if(moti(3))
        
        xv = vor{1};
        yv = vor{2};
        G = vor{3};
        n_vor = length(G);
 
        for i=1:1:length(x)

            for j=1:1:length(y)
                
                for k=1:1:n_vor

                    c = G(k)/(2*pi);
                    r = sqrt((x(i)-xv(k))^2+(y(j)-yv(k))^2); 
                    theta = atan2(y(j)-yv(k),x(i)-xv(k));

                    PHIV(i,j) = PHIV(i,j) - c*theta;
                    PSIV(i,j) = PSIV(i,j) + c*log(r);
                    UV(i,j) = UV(i,j) + c*(y(j)-yv(k))/r^2;
                    VV(i,j) = VV(i,j) - c*(x(i)-xv(k))/r^2;
                end
                
            end
        end
    end
    
    % --- MOTO DIPOLO ---
    PSID = zeros(length(x),length(y));
    PHID = zeros(length(x),length(y));
    UD = zeros(length(x),length(y));
    VD = zeros(length(x),length(y));
    
    if(moti(4))
        
        xd = dip{1};
        yd = dip{2};
        Kd = dip{3};
        n_dip = length(Kd);
 
        for i=1:1:length(x)

            for j=1:1:length(y)
                
                for k=1:1:n_dip

                    c = Kd/(2*pi);
                    r = sqrt((x(i)-xd(k))^2+(y(j)-yd(k))^2); 

                    PHID(i,j) = PHID(i,j) + c*(x(i)-xd(k))/r^2;
                    PSID(i,j) = PSID(i,j) - c*(y(j)-yd(k))/r^2;
                    UD(i,j) = UD(i,j) - c*((x(i)-xd(k))^2-(y(j)-yd(k))^2)/r^4;
                    VD(i,j) = VD(i,j) - c*(2*(x(i)-xd(k))*(y(j)-yd(k)))/r^4;
                end
                
            end
        end
    end
    
    %--- COSTANTI ---
    
    if(moti(1) && moti(2) && n_sor == 1) %Mezzo Corpo di Rankine
        PSIS = PSIS - ones(length(x),length(y))*(Qs/2);
    end
    if(moti(1) && moti(2) && moti(3) && n_sor == 2) %Cilindro con Circuitazione
        PSIS = PSIS - ones(length(x),length(y))*(G/4*pi)*log(Kd/(2*pi*vinf));
    end

    % --- SOVRAPPOSIZIONE DEGLI EFFETTI ---
    
    PSI = PSII + PSIS + PSIV + PSID;
    PHI = PHII + PHIS + PHIV + PHID;
    U = UI + US + UV + UD;
    V = VI + VS + VV + VD;

    % --- CAMPO VELOCITA ---
    
    figure(1); hold on;
    
    axis([-3 3 -3 3]); axis equal;
    streamslice(X,Y,U.',V.');
    title('Campo Velocita','FontSize',18);
    xlabel('x','FontSize',18);
    ylabel('y','Rotation',0,'FontSize',18);
    
    hold off;
    
    % --- LINEE EQUICORRENTE e EQUIPOTENZIALI---
    
    %minlim = ceil(min(PSI(:)));
    %maxlim = ceil(max(PSI(:)));
    
    figure(2); hold on;
    
    Z=-100:0.5:100;
    axis([-3 3 -3 3]); axis equal;
    title('Campo Corrente \psi e Potenziale \phi ','FontSize',18);
    contour(X,Y,PSI.',Z,'LineWidth',1,'ShowText','off','LineColor','g');
    contour(X,Y,PHI.',Z,'LineWidth',1,'ShowText','off','LineColor','r');
    xlabel('x','FontSize',18);
    ylabel('y','Rotation',0,'FontSize',18);
    legend('Linee Isocorrente','Linee Isopotenziali');
    
    hold off;
    
    % --- DISEGNI ---
    
    if(moti(1) && moti(4) && n_dip==1)
        db=0.1;
        R = sqrt(Kd/(2*pi*vinf));
        [ P ] = draw_circle( 0,db,xd,yd,R,2*pi);
        figure(1); hold on;
        patch(P(:,1)',P(:,2)',[0.4 0.8 0]);
        hold off;
        figure(2); hold on;
        patch(P(:,1)',P(:,2)',[0.4 0.8 0]);
        hold off;
    end
end